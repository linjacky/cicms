<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Page_head
 *
 * @author Lin Jacky
 * @package cicms
 */
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php echo $meta_title; ?></title>
		<!-- Bootstrap -->
		<link href="<?php echo site_url('css/bootstrap.min.css'); ?>" rel="stylesheet" media="screen">
		<link href="<?php echo site_url('css/style.css'); ?>" rel="stylesheet">
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script src="<?php echo site_url('js/bootstrap.min.js'); ?>"></script>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<!-- TinyMCE -->
		<script type="text/javascript" src="<?php echo site_url('js/tiny_mce/tiny_mce.js'); ?>"></script>
	</head>
	<body>
<?php
/* End of file page_head.php */
/* Location: ./application/views/components/page_head.php */