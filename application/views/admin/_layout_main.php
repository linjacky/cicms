<?php $this->load->view('admin/components/page_head');?>
	<body>
		<div class="navbar navbar-static-top navbar-inverse">
			<div class="navbar-inner">
				<a class="brand" href="<?php echo site_url('admin/dashboard'); ?>"><?php echo $meta_title; ?></a>
				<ul class="nav">
					<li class="active">
						<a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a>
					</li>
					<li>
						<a href="<?php echo site_url('admin/page'); ?>">Page</a>
					</li>
					<li>
						<a href="<?php echo site_url('admin/page/order'); ?>">Order Page</a>
					</li>
					<li>
						<a href="<?php echo site_url('admin/article'); ?>">News Article</a>
					</li>
					<li>
						<a href="<?php echo site_url('admin/user'); ?>">User</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row-fluid">
				<!-- Main column -->
				<div class="span9">
<?php 
if (isset($subview))
{
	$this->load->view($subview);
} else {
	echo('<h2>Page Name</h2>');
}
?>
				</div>
				<!-- Sidebar -->
				<div class="span3">
					<section>
						<?php echo mailto('Lin.ChiehJen@gmail.com', '<i class="icon-user"></i> Lin.ChiehJen@gmail.com'); ?><br />
						<?php echo anchor('admin/user/logout', '<i class="icon-off"></i> logout'); ?>
					</section>
				</div>
			</div>
		</div>
<?php $this->load->view('admin/components/page_foot');?>