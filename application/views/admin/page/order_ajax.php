<?php
//dump($pages);
echo get_ol($pages);

function get_ol($array, $child = FALSE)
{
	$str = '';
	
	if (count($array))
	{
		$str .= ($child === FALSE) ? '<ol class="sortable">' : '<ol>';
		
		foreach ($array as $item) {
			$str .= '<li id="list_' . $item['id'] . '">';
			$str .= '<div>' . $item['title'] . '</div>';
			
			// Do we have any children?
			if (isset($item['children']) && count($item['children']))
			{
				$str .= get_ol($item['children'], TRUE);
			}
			
			$str .= '</li>' . PHP_EOL;//PHP_EOL 換行常數;
		}
		
		$str .= '</ol>' . PHP_EOL;//PHP_EOL 換行常數;
	}
	
	return $str;
}
?>
<script>
$(document).ready(function(){

        $('.sortable').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            maxLevels: 2,
        });

    });
</script>
<?php
/* End of file order_ajax.php */
/* Location: ./application/views/admin/page/order_ajax.php */