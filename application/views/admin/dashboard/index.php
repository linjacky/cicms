<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Index
 *
 * @author Lin Jacky
 * @package cicms
 */
?>
<h2>Recent modified articles</h2>
<?php if(count($recent_articles)): ?>
	<ul>
<?php foreach ($recent_articles as $article): ?>
		<li>
<?php echo anchor('admin/article/edit/' . $article->id, e($article->title)) . ' - ' . date('Y-m-d', strtotime(e($article->updated))); ?>
		</li>
<?php endforeach; ?>
	</ul>
<?php endif; ?>
<?php 
/* End of file index.php */
/* Location: ./application/views/admin/dashboard/index.php */