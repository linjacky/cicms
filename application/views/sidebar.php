<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sidebar
 *
 * @author Lin Jacky
 * @package cicms
 */

echo anchor($news_archive_link, '+ News archive'); 
echo article_links($recent_news); 

/* End of file sidebar.php */
/* Location: ./application/views/sidebar.php */