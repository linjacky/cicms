<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Article
 *
 * @author Lin Jacky
 * @package cicms
 */
?>
<!-- Main contents -->
<div class="span9">
	<article>
		<h2><?php echo e($article->title);?></h2>
		<p class="pubdate"><?php echo e($article->pubdate);?></p>
		<?php echo $article->body;?>
	</article>
</div>
<!-- Sidebar -->
<div class="span3 sidebar">
	<h2>Recent news</h2>
<?php $this->load->view('sidebar');?>
</div>
<?php
/* End of file article.php */
/* Location: ./application/views/templates/article.php */