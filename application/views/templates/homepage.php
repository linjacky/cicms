<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Homepage
 *
 * @author Lin Jacky
 * @package cicms
 */
?>
<!-- Main contents -->
<div class="span9">
	<div class="row">
		<div class="span9"><?php if(isset($articles[0])) echo get_excerpt($articles[0]);?></div>
	</div>
	<div class="row">
		<div class="span5"><?php if(isset($articles[1])) echo get_excerpt($articles[1]);?></div>
		<div class="span4"><?php if(isset($articles[2])) echo get_excerpt($articles[2]);?></div>
	</div>
</div>
<!-- Sidebar -->
<div class="span3 sidebar">
	<h2>Recent news</h2>
<?php 
// Remove first 3 articles.
echo anchor($news_archive_link, '+ News archive'); 
$articles = array_slice($articles, 3);
echo article_links($articles); 
?>
</div>
<?php
/* End of file homepage.php */
/* Location: ./application/views/templates/homepage.php */