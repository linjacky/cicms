<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * News_archive
 *
 * @author Lin Jacky
 * @package cicms
 */
?>
<!-- Main contents -->
<div class="span9">
<?php if(isset($pagination)): ?>
	<section class="pagination"><?php echo $pagination;?></section>
<?php endif;?>
	<div class="row">
<?php if(count($articles)): ?>
<?php foreach($articles as $article): ?>
		<article class="span9"><?php echo get_excerpt($article);?><hr /></article>
<?php endforeach; endif;?>
	</div>
</div>
<!-- Sidebar -->
<div class="span3 sidebar">
	<h2>Recent news</h2>
<?php $this->load->view('sidebar');?>
</div>
<?php
/* End of file news_archive.php */
/* Location: ./application/views/templates/news_archive.php */