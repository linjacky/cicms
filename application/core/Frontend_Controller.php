<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Frontend_Controller
 *
 * @author Lin Jacky
 * @package cicms
 */
class Frontend_Controller extends MY_Controller {
  
	/**
	 * Constructor. 
	 */
	function __construct()
	{
	  parent::__construct();
	  log_message('debug', 'Frontend_Controller initialised');
	  
	  $this->load->model('pages');
	  // Remove all articles model loads.
	  $this->load->model('articles');
	  
	  $this->data['menu'] = $this->pages->get_nested();
	  $this->data['news_archive_link'] = $this->pages->get_archive_link();
	  $this->data['meta_title'] = config_item('site_name');
	}
	
} 
/* End of file Frontend_Controller.php */
/* Location: ./application/libraries/Frontend_Controller.php */