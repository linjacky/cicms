<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MY_Controller
 *
 * @author Lin Jacky
 * @package cicms
 */
class MY_Controller extends CI_Controller {
  
	public $data = array();
	protected $_model = '';
	/**
	 * Constructor. 
	 */
	function __construct()
	{
		parent::__construct();
		$this->data['error'] = array();
		//在 ../contig/cms_congig.php
		$this->data['site_name'] = config_item('site_name');
		
		//自動載入複數名稱的Model;
		$this->load->helper('inflector');
		$_model = plural(strtolower(get_class($this)));//預設複數model_controller名稱;
		$_model_file = APPPATH . 'models/' . $_model . '.php';
		if(file_exists($_model_file) && is_file($_model_file)) //確認 model 檔案存在
		{
			$this->load->model($_model); // 自動載入複數名稱的Model;
			$this->_model = $_model;//儲存 Model 名稱的Model;
		}
		log_message('debug', 'MY_Controller initialised');
	}
} 
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */