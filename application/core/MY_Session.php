<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MY_Session
 *
 * @author Lin Jacky
 * @package cicms
 */
class MY_Session extends CI_Session {
  
	/**
	 * Constructor. 
	 */
	function __construct()
	{
	  parent::__construct();
	  log_message('debug', 'MY_Session initialised');
	}
	
	function sess_update()
	{
		// Listen to HTTP_X_REQUESTED_WITH
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] !== 'XMLHttpRequest')
		{
			parent::sess_update();
		}
	}

} 
/* End of file MY_Session.php */
/* Location: ./application/core/MY_Session.php */