<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MY_Model
 *
 * @author Lin Jacky
 * @package cicms
 */
class MY_Model extends CI_Model {
	
	protected $_table_name = '';
	protected $_primary_key = 'id';//預設主鍵欄位名;
	protected $_primary_filter = 'intval';//主鍵驗證函數;
	protected $_order_by = '';//預設排序;
	public $rules = array();//預設驗證規則;
	//protected $_timestamps = FALSE;
	
	/**
	 * Constructor. 
	 */
	function __construct()
	{
		parent::__construct();
		if (empty($this->_table_name)) 
		{
			$this->_table_name = get_class($this);
		}
		log_message('debug', 'MY_Model initialised');
	}

	public function array_from_post($fields)
	{
		$data = array();
		foreach ($fields as $field) {
			$data[$field] = $this->input->post($field);
		}
		return $data;
	}

	public function get($id = NULL, $single = FALSE)
	{
		$method = 'row';
		//
		if ($id != NULL) 
		{
			//過濾主鍵
			$id = $this->_filter($id);
			//
			$this->db->where($this->_primary_key, $id);//預設主鍵欄位名;
		}
		elseif($single == FALSE)
		{
			$method = 'result';//多筆;
		}
		//
		if (!count($this->db->ar_orderby)) // 預設排序陣列 ar_orderby 為 0;
		{ 
			$this->db->order_by($this->_order_by);
		}
		return $this->db->get($this->_table_name)->$method();//row() 或 result();
	}
	
	public function get_by($where, $single = FALSE)
	{
		$method = 'row';
		//
		$this->db->where($where);
		//
		if($single == FALSE)
		{
			$method = 'result';
		}
		return $this->db->get($this->_table_name)->$method();
	}
	
	public function save($data, $id = NULL)
	{
		
		//插入 Insert;
		if ($id === NULL) 
		{
			if (! isset($data[$this->_primary_key]) || $data[$this->_primary_key] == NULL) 
			{
				//設置 timestamps;
				$data['created'] = date('Y-m-d H:i:s');
				//
				$this->db->insert($this->_table_name, $data);
				//取回插入資料之主鍵;
				$id = $this->db->insert_id();
			}
		}
		//更新 Update;
		else {
			//過濾主鍵
			$id = $this->_filter($id);
			$this->db->update($this->_table_name, $data, array('id' => $id));
		}
		//回傳主鍵值;
		return $id;
	}
	
	public function delete($id)
	{
		//過濾主鍵
		$id = $this->_filter($id);
		if (! $id)
		{
			return FALSE;
		}
		else 
		{
			$this->db->limit(1);
			$this->db->delete($this->_table_name, array('id' => $id));
			return $id;
		}
	}
	
	//主鍵過濾器;
	private function _filter($key)
	{
		$filter = $this->_primary_filter;//主鍵驗證函數;
		return $filter($key);//驗證引數;
	}
} 
/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */