<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Admin_Controller
 *
 * @author Lin Jacky
 * @package cicms
 */
class Admin_Controller extends MY_Controller {

	/**
	 * Constructor.
	 */
	function __construct() 
	{
		parent::__construct();
		$this -> data['meta_title'] = 'My awesome CMS';
		$this -> load -> helper('form');
		$this -> load -> library('form_validation');
		$this -> load -> library('session');
		$this -> load -> model('users');
		//登入檢查;
		$exception_urls = array( //例外網址;
			'admin/user/login',
			'admin/user/logout',
		);
		if (in_array(uri_string(), $exception_urls) == FALSE)
		{
			if ($this->users->loggedin() == FALSE) 
			{
				redirect('admin/user/login');
			}
		}		

		log_message('debug', 'Admin_Controller initialised');
	}

}

/* End of file Admin_Controller.php */
/* Location: ./application/core/Admin_Controller.php */