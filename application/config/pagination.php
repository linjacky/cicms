<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Pagination
 *
 * @author Lin Jacky
 * @package cicms
 */

$config['full_tag_open'] = '<ul>';
$config['full_tag_close'] = '</ul>';
$config['first_link'] = FALSE;
$config['last_link'] = FALSE;
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li><a href="#" class="cur_tag">';
$config['cur_tag_close'] = '</a></li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';

/* End of file pagination.php */
/* Location: ./application/config/pagination.php */