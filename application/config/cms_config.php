<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Cms_config
 *
 * @author Lin Jacky
 * @package cicms
 */

$config['site_name'] = 'CodeIgniter CMS';//'My awesome site';

/* End of file cms_config.php */
/* Location: ./application/config/cms_config.php */