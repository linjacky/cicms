<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users
 *
 * @author Lin Jacky
 * @package cicms
 */
class Users extends MY_Model {
	
	//protected $_table_name = '';
	//protected $_primary_key = 'id';//預設主鍵欄位名;
	//protected $_primary_filter = 'intval';//主鍵驗證函數;
	protected $_order_by = 'name';//預設排序;
	public $rules = array(
		'email' => array(
			'field' => 'email', 
			'label' => 'Email', 
			'rules' => 'trim|required|valid_email|xss_clean',
		),
		'password' => array(
			'field' => 'password', 
			'label' => 'Password', 
			'rules' => 'trim|required|xss_clean',
		),
	);
	public $rules_admin = array(
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|xss_clean',
		),
		'email' => array(
			'field' => 'email', 
			'label' => 'Email', 
			'rules' => 'trim|required|valid_email|callback__unique_email|xss_clean',
		),
		'password' => array(
			'field' => 'password', 
			'label' => 'Password', 
			'rules' => 'trim|matches[password_confirm]',//注意是matches
		),
		'password_confirm' => array(
			'field' => 'password_confirm', 
			'label' => 'Confirm Password', 
			'rules' => 'trim|matches[password]',//注意要重複一次matches?
		),
	);
	/**
	 * Constructor. 
	 */
	function __construct()
	{
		parent::__construct();
		log_message('debug', 'Users initialised');
	}
	
	public function login()
	{
		$user = $this->get_by(array(
			'email' => $this->input->post('email'),
			'password' => $this->hash($this->input->post('password')),
		), TRUE);
		//
		if (count($user))
		{
			//用戶登入;
			$data = array(
				'name' => $user->name,
				'email' => $user->email,
				'id' => $user->id,
				'loggedin' => TRUE,
			);
			//
			$this->session->set_userdata($data);
		}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
	}
	
	public function loggedin()
	{
		return (bool) $this->session->userdata('loggedin');
	}
	
	public function get_new()
	{
		$user = new stdClass();
		$user->name = '';
		$user->email = '';
		$user->password = '';
		return $user;
	}
	
	public function hash($string)
	{
		return md5($string . config_item('encryption_key'));
	}
} 
/* End of file users.php */
/* Location: ./application/models/users.php */