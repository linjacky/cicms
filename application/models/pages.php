<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Pages
 *
 * @author Lin Jacky
 * @package cicms
 */
class Pages extends MY_Model {
	//protected $_table_name = 'pages';
	//protected $_primary_key = 'id';//預設主鍵欄位名;
	//protected $_primary_filter = 'intval';//主鍵驗證函數;
	protected $_order_by = 'order';//預設排序;
	//protected $_rules = array();//
	//protected $_timestamps = FALSE;
	public $rules = array(
		'parent_id' => array(
			'field' => 'parent_id', 
			'label' => 'Parent', 
			'rules' => 'trim|intval',
		),
		'title' => array(
			'field' => 'title', 
			'label' => 'Title', 
			'rules' => 'trim|required|max_length[128]|xss_clean',
		),
		'slug' => array(
			'field' => 'slug', 
			'label' => 'Slug', 
			'rules' => 'trim|required|max_length[128]|url_title|callback__unique_slug|xss_clean',
		),
		'body' => array(
			'field' => 'body', 
			'label' => 'Body', 
			'rules' => 'required',
		),
		'template' => array(
			'field' => 'template', 
			'label' => 'Template', 
			'rules' => 'trim|required|xss_clean',
		),
	);
	/**
	 * Constructor. 
	 */
	/*
	function __construct()
	{
		parent::__construct();
		log_message('debug', 'Pages initialised');
	}
	 */
	public function get_new()
	{
		$page = new stdClass();
		$page->title = '';
		$page->slug = '';
		$page->body = '';
		$page->parent_id = 0;
		$page->template = 'page';
		return $page;
	}
	
	public function get_archive_link($value='')
	{
		$page = parent::get_by(array('template' => 'news_archive'),TRUE);
		return isset($page->slug) ? $page->slug : '';
	}
	
	public function delete($id)
	{
		// Delete a page.
		parent::delete($id);
		// Reset parent ID for its children.
		$this->db->set(array('parent_id' => 0))->where('parent_id', $id)->update($this->_table_name);
	}
	
	public function save_order($pages){
		if (count($pages))
		{
			foreach ($pages as $order => $page)
			{
				//dump($page);
				if ($page['item_id'] != '')
				{
					$data = array('parent_id' => (int) $page['parent_id'], 'order' => $order);
					$this->db->set($data)->where($this->_primary_key, $page['item_id'])->update($this->_table_name);
					//echo '<pre>' . $this->db->last_query() . '</pre>';
				}
			}
		}
	}
	
	public function get_nested()
	{
		//$pages = $this->db->get('pages')->result_array();
		$pages = $this->db->order_by($this->_order_by)->get($this->_table_name)->result_array();
		
		$array = array();
		foreach ($pages as $page) {
			if (!$page['parent_id'])
			{
				$array[$page['id']] = $page;
			}
			else 
			{
				$array[$page['parent_id']]['children'][] = $page;
			}
		}
		return $array;
	}
	
	public function get_with_parent($id = NULL, $single = FALSE)
	{
		$this->db->select('pages.*, p.slug as parent_slug, p.title as parent_title');
		$this->db->join('pages as p', 'pages.parent_id=p.id', 'left');//Left Join by parent_id.
		return parent::get($id, $single);
	}
	
	public function get_no_parents()
	{
		// Retrieve page without parents.
		$this->db->select('id, title');
		$this->db->where('parent_id', 0);
		$pages = parent::get();
		
		// Return key => value pair array.
		$array = array(0 => 'No parent');
		if (count($pages))
		{
			foreach ($pages as $page) {
				$array[$page->id] = $page->title;
			}
		}
		return $array;
	}
} 
/* End of file pages.php */
/* Location: ./application/models/pages.php */