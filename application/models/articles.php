<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Articles
 *
 * @author Lin Jacky
 * @package cicms
 */
class Articles extends MY_Model {
	//protected $_table_name = 'articles';
	//protected $_primary_key = 'id';//預設主鍵欄位名;
	//protected $_primary_filter = 'intval';//主鍵驗證函數;
	protected $_order_by = 'pubdate desc, id desc';//預設排序;
	//protected $_rules = array();//
	//protected $_timestamps = TRUE;
	public $rules = array(
		'pubdate' => array(
			'field' => 'pubdate', 
			'label' => 'Publication date', 
			'rules' => 'trim|required|exact_length[10]|xss_clean',
		),
		'title' => array(
			'field' => 'title', 
			'label' => 'Title', 
			'rules' => 'trim|required|max_length[128]|xss_clean',
		),
		'slug' => array(
			'field' => 'slug', 
			'label' => 'Slug', 
			'rules' => 'trim|required|max_length[128]|url_title|xss_clean',
		),
		'body' => array(
			'field' => 'body', 
			'label' => 'Body', 
			'rules' => 'required',
		),
	);
	/**
	 * Constructor. 
	 */
	/*
	function __construct()
	{
		parent::__construct();
		log_message('debug', 'articles initialised');
	}
	 */
	public function get_new()
	{
		$article = new stdClass();
		$article->title = '';
		$article->slug = '';
		$article->body = '';
		$article->pubdate = date('Y-m-d');
		return $article;
	}
	
	public function set_published()
	{
		// Replace all instance of pubdate.
		$this->db->where('pubdate <=', date('Y-m-d'));
	}
	
	public function get_recent($limit = 3)
	{
		$limit = (int) $limit;
		$this->set_published();
		$this->db->limit($limit);
		return parent::get();
	}
} 
/* End of file articles.php */
/* Location: ./application/models/rticles.php */