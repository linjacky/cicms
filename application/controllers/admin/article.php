<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Article
 *
 * @author Lin Jacky
 * @package cicms
 */
class Article extends Admin_Controller {
  
	/**
	 * Constructor. 
	 */
	function __construct()
	{
	  parent::__construct();
	  log_message('debug', 'Article initialised');
	}
	
	public function index()
	{
		// Retrieve all articles. 取回所有用戶資料。
		$this->data['articles'] = $this->articles->get();
		
		// Load Layout & View. 載入樣板與視圖。
		$this->data['subview'] = 'admin/article/index';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	public function edit($id = NULL)
	{
		// Retrieve a article. 取得一個特定頁面。
		if($id)
		{
			$this->data['article'] = $this->articles->get($id);
			count($this->data['article']) || $this->data['errors'][] = "article cannot be found!";
		}
		else 
		{
			$this->data['article'] = $this->articles->get_new();
		}
		
		// Set up the form & validation rules. 設定表單與驗證規則。
		$rules = $this->articles->rules;
		$this->form_validation->set_rules($rules);
		
		// Process the form. 處理表單資料。
		if ($this->form_validation->run() == TRUE)
		{
			$data = $this->articles->array_from_post(array('title', 'slug', 'body', 'pubdate',));
			$this->articles->save($data, $id);
			redirect('admin/article');
		}
		
		// Load Layout & View. 載入樣板與視圖。
		$this->data['subview'] = 'admin/article/edit';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	public function delete($id)
	{
		$this->articles->delete($id);
		redirect('admin/article');
		
	}
} 
/* End of file article.php */
/* Location: ./application/controllers/admin/article.php */