<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Page
 *
 * @author Lin Jacky
 * @package cicms
 */
class Page extends Admin_Controller {
  
	/**
	 * Constructor. 
	 */
	function __construct()
	{
	  parent::__construct();
	  log_message('debug', 'Page initialised');
	}
	
	public function index()
	{
		// Retrieve all pages. 取回所有用戶資料。
		$this->data['pages'] = $this->pages->get_with_parent();
		
		// Load Layout & View. 載入樣板與視圖。
		$this->data['subview'] = 'admin/page/index';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	public function order($value='')
	{
		$this->data['sortable'] = TRUE;
		$this->data['subview'] = 'admin/page/order';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	public function order_ajax($value='')
	{
		// Save order from ajax call;
		if (isset($_POST['sortable']))
		{
			$this->pages->save_order($_POST['sortable']);
		}
		
		// Retrieve all pages. 取回所有用戶資料。
		$this->data['pages'] = $this->pages->get_nested();
		
		// Load View. 僅載入視圖。
		$this->load->view('admin/page/order_ajax', $this->data);
	}
	
	public function edit($id = NULL)
	{
		// Retrieve a page. 取得一個特定頁面。
		if($id)
		{
			$this->data['page'] = $this->pages->get($id);
			count($this->data['page']) || $this->data['errors'][] = "page cannot be found!";
		}
		else 
		{
			$this->data['page'] = $this->pages->get_new($id);
		}
		
		// Parent pages for dropdown menu. 設定父頁面下拉遠單。
		$this->data['pages_no_parents'] = $this->pages->get_no_parents();
		//dump($this->data['pages_no_parents']);
		
		// Set up the form & validation rules. 設定表單與驗證規則。
		$rules = $this->pages->rules;
		$this->form_validation->set_rules($rules);
		
		// Process the form. 處理表單資料。
		if ($this->form_validation->run() == TRUE)
		{
			$data = $this->pages->array_from_post(array(
				'title', 
				'slug', 
				'body', 
				'parent_id',
				'template',
			));
			$this->pages->save($data, $id);
			redirect('admin/page');
		}
		
		// Load Layout & View. 載入樣板與視圖。
		$this->data['subview'] = 'admin/page/edit';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	public function delete($id)
	{
		$this->pages->delete($id);
		redirect('admin/page');
		
	}
	
	public function _unique_slug($str)
	{
		// Do NOT validate if slug already existed, Slug重複則不能通過驗證，
		// UNLESS it's the slug for the current page. 除非是目前頁面的slug。
		
		$id = $this->uri->segment(4);
		$this->db->where('slug', $this->input->post('slug'));
		!$id || $this->db->where('id !=', $id);
		$page = $this->pages->get();
		
		if (count($page))
		{
			$this->form_validation->set_message('_unique_slug', '%s should be unique!');
			return FALSE;
		}
		return TRUE;
	}
} 
/* End of file page.php */
/* Location: ./application/controllers/admin/page.php */