<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User
 *
 * @author Lin Jacky
 * @package cicms
 */
class User extends Admin_Controller {
  
	/**
	 * Constructor. 
	 */
	function __construct()
	{
	  parent::__construct();
	  log_message('debug', 'User initialised');
	}
	
	public function index()
	{
		// Retrieve all users. 取回所有用戶資料。
		$this->data['users'] = $this->users->get();
		
		// Load Layout & View. 載入樣板與視圖。
		$this->data['subview'] = 'admin/user/index';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	public function edit($id = NULL)
	{
		// Retrieve a user. 取得一個特定用戶。
		if($id)
		{
			$this->data['user'] = $this->users->get($id);
			count($this->data['user']) || $this->data['errors'][] = "User cannot be found!";
		}
		else 
		{
			$this->data['user'] = $this->users->get_new($id);
		}
		
		// Set up the form & validation rules. 設定表單與驗證規則。
		$rules = $this->users->rules_admin;
		$id || $rules['password'] .= '|required';
		$this->form_validation->set_rules($rules);
		
		// Process the form. 處理表單資料。
		if ($this->form_validation->run() == TRUE)
		{
			$data = $this->users->array_from_post(array('name', 'email', 'password'));
			$data['password'] = $this->users->hash($data['password']);
			$this->users->save($data, $id);
			redirect('admin/user');
		}
		
		// Load Layout & View. 載入樣板與視圖。
		$this->data['subview'] = 'admin/user/edit';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	public function delete($id)
	{
		$this->users->delete($id);
		redirect('admin/user');
		
	}
	
	public function login()
	{
		// Redirect a user if he's already logged in. 如果用戶登入則重新導向。
		$dashboard = 'admin/dashboard';
		$this->users->loggedin() == FALSE || redirect($dashboard);
		
		// Set form. 設定表單。
		$rules = $this->users->rules;
		$this->form_validation->set_rules($rules);
		
		// Process form. 處理表單。
		if ($this->form_validation->run() == TRUE)
		{
			//可以登入;
			if ($this->users->login() == TRUE)
			{
				redirect($dashboard);
			}
			else 
			{
				$this->session->set_flashdata('error','The email/password combination dose not exist!');
				redirect('admin/user/login', 'refresh');
			}
		}
		
		// Load view. 載入視圖。
		$this->data['subview'] = 'admin/user/login';
		$this->load->view('admin/_layout_modal', $this->data);
	}

	public function logout()
	{
		$this->users->logout();
		redirect('admin/user/login');
	}
	
	public function _unique_email($address)
	{
		// Do NOT validate if email already existed, Email重負則不能通過驗證，
		// UNLESS it's the email for the current user. 除非是目前用戶的email。
		
		$id = $this->uri->segment(4);
		$this->db->where('email', $this->input->post('email'));
		!$id || $this->db->where('id !=', $id);
		$user = $this->users->get();
		
		if (count($user))
		{
			$this->form_validation->set_message('_unique_email', '%s should be unique!');
			return FALSE;
		}
		return TRUE;
	}
} 
/* End of file user.php */
/* Location: ./application/controllers/admin/user.php */