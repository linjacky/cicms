<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Migration
 *
 * @author Lin Jacky
 * @package cicms
 */
class Migration extends Admin_Controller {
  
	/**
	 * Constructor. 
	 */
	function __construct()
	{
	  parent::__construct();
	  log_message('debug', 'Migration initialised');
	}
	
	function index()
	{
		$this->load->library('migration');
		if(! $this->migration->current()) {
			show_error($this->migration->error_string());
		}
		else {
			//var_dump($this->config);
			echo('Migration is successful!');
		}
	}

} 
/* End of file migration.php */
/* Location: ./application/controllers/admin/migration.php */