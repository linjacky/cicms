<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dashboard
 *
 * @author Lin Jacky
 * @package cicms
 */
class Dashboard extends Admin_Controller {
  
	/**
	 * Constructor. 
	 */
	public function __construct()
	{
	  parent::__construct();
	  log_message('debug', 'Dashboard initialised');
	}
	
	public function index()
	{
		// Fetch recent modified articles.
		$this->load->model('articles');
		$this->db->order_by('updated desc');
		$this->db->limit(5);
		$this->data['recent_articles'] = $this->articles->get();
		
		$this->data['subview'] = 'admin/dashboard/index';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	public function modal()
	{
		$this->load->view('admin/_layout_modal', $this->data);
	}

} 
/* End of file dashboard.php */
/* Location: ./application/controllers/admin/dashboard.php */