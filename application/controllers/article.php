<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Article
 *
 * @author Lin Jacky
 * @package cicms
 */
class Article extends Frontend_Controller {
  
	/**
	 * Constructor. 
	 */
	function __construct()
	{
	  parent::__construct();
	  
	  $this->articles->get_recent();
	  log_message('debug', 'Article initialised');
	  $this->data['recent_news'] = $this->articles->get_recent();
	}
	
	function index($id , $slug)
	{
		// Fetch the article;
		$this->articles->set_published();
		$this->data['article'] = $this->articles->get($id);
		
		// Return 404 if not found.
		count($this->data['article']) || show_404(uri_string());
		
		// Redirect if slug was incorrect.
		if($this->uri->segment(3) != $this->data['article']->slug)
		{
			redirect('article/' . $this->data['article']->id . '/' . $this->data['article']->slug, 'locatior', '301');
		}
		
		
		// Load View.
		add_meta_title($this->data['article']->title);
		$this->data['subview'] = 'article';
		$this->load->view('_main_layout', $this->data);
	}

} 
/* End of file article.php */
/* Location: ./application/controllers/article.php */