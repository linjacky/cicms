<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Page
 *
 * @author Lin Jacky
 * @package cicms
 */
class Page extends Frontend_Controller {
  
	/**
	 * Constructor. 
	 */
	public function __construct()
	{
	  parent::__construct();
	  //$this->load->model('pages');
	  log_message('debug', 'Page initialised');
	}
	
	public function index()
	{
		// Fetch the page data.
		$this->data['page'] = $this->pages->get_by(array('slug' => (string) $this->uri->segment(1)), TRUE);
		count($this->data['page']) || show_404(current_url());
		
		add_meta_title($this->data['page']->title);
		
		// Fecth the page template.
		$method = '_' . $this->data['page']->template;
		if (method_exists($this, $method)) {
			$this->$method();
		}
		else {
			log_message('error', 'Cannot load the template ' . $methos . ' in the file' . __FILE__ . ' at ' . __LINE__);
			show_error('Cannot load the template ' . $method);
		}
		
		// Load the view.
		$this->data['subview'] = $this->data['page']->template;
		$this->load->view('_main_layout', $this->data);
	}
	
	private function _page()
	{
		$this->data['recent_news'] = $this->articles->get_recent();
	}
	
	private function _homepage()
	{
		$this->articles->set_published();
		$this->db->limit(6);
		$this->data['articles'] = $this->articles->get();
	}
	
	private function _news_archive()
	{
		$this->data['recent_news'] = $this->articles->get_recent();
		
		// Count all articles.
		$this->articles->set_published();
		$count = $this->db->count_all_results('articles');
		//dump($count);
		// Set up pagination.
		$perpage = 4;
		$offset = 0;
		if ($count > $perpage) {
			$this->load->library('pagination');
			$config['base_url'] = site_url($this->uri->segment(1) . '/');
			$config['total_rows'] = $count;
			$config['per_page'] = $perpage;
			$config['uri_segment'] = 2;
			// config in ./application/config/pagination.php
			$this->pagination->initialize($config); 
			$this->data['pagination'] = $this->pagination->create_links();
			$offset = intval($this->uri->segment(2));
		}
		else {
			$this->data['pagination'] = '';
		}
		//dump($this->data['pagination']);
		// Fetch articles.
		$this->articles->set_published();
		$this->db->limit($perpage, $offset);
		$this->data['articles'] = $this->articles->get();
		//dump(count($this->data['articles']));
		//echo '<pre>' . $this->db->last_query() . '</pre>';
	}
} 
/* End of file page.php */
/* Location: ./application/controllers/page.php */