<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 006_add_template_to_pages
 *
 * @author Lin Jacky
 * @package cicms
 */
class Migration_Add_template_to_pages extends CI_Migration {
	public function up()
	{
		$fileds = array(
			'template' => array(
				'type' => 'VARCHAR',
				'constraint' => 25,
			),
		);
		$this->dbforge->add_column('pages', $fileds);
	}

	public function down()
	{
		$this->dbforge->drop_column('pages', 'template');
	}

} 
/* End of file 006_add_template_to_pages.php */
/* Location: ./application/migrations/006_add_template_to_pages.php */