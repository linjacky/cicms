<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 001_create_users
 *
 * @author Lin Jacky
 * @package cicms
 */
class Migration_Create_users extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '32',
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '32',
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '32',
			),
			'created' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated' => array(
				'type' => 'TIMESTAMP',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('users');
		//加入兩個唯一鍵：email, name;
		$sql1 = 'CREATE UNIQUE INDEX email ON users (email)';
		$sql2 = 'CREATE UNIQUE INDEX name ON users (name)';
        $this->db->query($sql1);
		$this->db->query($sql2);
	}

	public function down()
	{
		$this->dbforge->drop_table('users');
	}

} 
/* End of file 001_create_users.php */
/* Location: ./application/migrations/001_create_users.php */