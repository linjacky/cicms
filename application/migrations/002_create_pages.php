<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 002_create_pages
 *
 * @author Lin Jacky
 * @package cicms
 */
class Migration_Create_pages extends CI_Migration {
  
	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'title' => array(
				'type' => 'VARCHAR',
				'constraint' => '128',
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '128',
			),
			'order' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'body' => array(
				'type' => 'TEXT',
			),
			'created' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated' => array(
				'type' => 'TIMESTAMP',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('pages');
	}

	public function down()
	{
		$this->dbforge->drop_table('pages');
	}

} 
/* End of file 002_create_pages.php */
/* Location: ./application/migrations/002_create_pages.php */