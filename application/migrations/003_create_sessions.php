<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 003_create_sessions
 *
 * @author Lin Jacky
 * @package cicms
 */
class Migration_Create_sessions extends CI_Migration {
  
	function up()
	{
		$this->dbforge->add_field(array(
			'session_id VARCHAR(40) DEFAULT \'0\' NOT NULL',
			'ip_address VARCHAR(45) DEFAULT \'0\' NOT NULL',
			'user_agent VARCHAR(120) NOT NULL',
			'last_activity INT(10) UNSIGNED DEFAULT 0 NOT NULL',
			'user_data TEXT NOT NULL',
		));
		$this->dbforge->add_key('session_id', TRUE);
		$this->dbforge->add_key('last_activity');
		$this->dbforge->create_table('ci_sessions');
	}

	function down()
	{
		$this->dbforge->drop_table('ci_sessions');
	}
	
} 
/* End of file 003_create_sessions.php */
/* Location: ./application/migrations/003_create_sessions.php */